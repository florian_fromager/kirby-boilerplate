"use strict";

var _pjaxEsm = _interopRequireDefault(require("./pjax.esm.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

// GLOBAL FUNCTIONS
var html = document.documentElement;
var body = document.body;

var contain = function contain(e, slug) {
  return e.classList.contains(slug);
};

function modifyState(url) {
  var stateObj = {
    id: "100"
  };
  window.history.replaceState(stateObj, "", url);
}

var figureIntersectionObserver = new IntersectionObserver(function (allObserved) {
  allObserved.forEach(function (observed) {
    if (!observed.isIntersecting) return;
    var img = observed.target;
    img.src = img.dataset.src;
    delete img.dataset.src;
    delete img.dataset.oc;
    img.classList.remove("lazy");
    figureIntersectionObserver.unobserve(img);
  });
});

var browserDetect = function browserDetect() {
  var userAgent = navigator.userAgent;
  return userAgent.match(/safari/i) && !userAgent.match(/chrome|chromium|crios/i);
};

var setScrollbar = function setScrollbar() {
  // Create the measurement node
  var scrollDiv = document.createElement("div");
  scrollDiv.className = "scrollbar-measure";
  body.appendChild(scrollDiv);
  var sb = scrollDiv.offsetWidth - scrollDiv.clientWidth;
  html.style.setProperty('--sb', "".concat(sb, "px"));
  body.removeChild(scrollDiv);
}; // POLYFILL FUNCTIONS


var pureAJAX = function pureAJAX(e) {
  var target, href, request;
  return regeneratorRuntime.async(function pureAJAX$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          target = e.target;
          href = target.href;
          request = new XMLHttpRequest();
          request.open("GET", "".concat(href), true);

          request.onload = function () {
            var targetHtml = new DOMParser().parseFromString(request.responseText, 'text/html');
            html.querySelector("#content").innerHTML = targetHtml.querySelector("#content").innerHTML;
            modifyState(href);
            var mainTitle = html.querySelector("h1");
            var nextTitle = targetHtml.querySelector("#content h2");
            if (nextTitle) document.title = "".concat(mainTitle.innerText, " \u2014 ").concat(nextTitle.innerText);
          };

          request.send();
          setTimeout(function () {
            return pageInit;
          }, 1000);

        case 7:
        case "end":
          return _context.stop();
      }
    }
  });
};

var noWebpObserver = function noWebpObserver(e) {
  e.forEach(function (img) {
    img.src = img.dataset.oc;
    delete img.dataset.src;
    delete img.dataset.oc;
    img.classList.remove("lazy");
  });
};

var setUnits = function setUnits() {
  var vh = window.innerHeight * 0.01;
  html.style.setProperty('--vh', "".concat(vh, "px"));
};

var orthoTypo = function orthoTypo(str) {
  if (_typeof(str) === "object") str = str.innerText;
  var ent = '&#8239;';
  var stArr = [[' ;', "".concat(ent, ";")], [' :', "".concat(ent, ":")], [' !', "".concat(ent, "!")], [' ?', "".concat(ent, "?")], ['« ', "\xAB".concat(ent)], [' »', "".concat(ent, "\xBB")], [' & ', " &amp;&nbsp;"]];

  for (var i = 0; i < stArr.length; i++) {
    var e = stArr[i];
    if (str.includes(e[0])) str = str.replaceAll(e[0], e[1]);
  }

  return str;
}; // GLOBAL INIT


var pageInit = function pageInit() {
  if (!contain(html, "touch")) {
    console.log("desktop");
    var pjax = new _pjaxEsm["default"]({
      elements: "a",
      selectors: ["#content"]
    });
    console.log(pjax);
  } else {
    if (contain(html, "mobile")) {
      console.log("mobile");
      var links = document.querySelectorAll("a");

      for (var i = 0; i < links.length; i++) {
        var link = links[i];
        link.addEventListener("click", function (e) {
          return pureAJAX(e);
        });
      }
    } else {
      var _pjax = new _pjaxEsm["default"]({
        elements: "a",
        selectors: ["#content"]
      });
    }
  }

  setTimeout(function () {
    var lazyImg = document.querySelectorAll(".lazy");

    if (contain(html, "webp")) {
      if (lazyImg.length !== 0 && window.IntersectionObserver) {
        lazyImg.forEach(function (img) {
          return figureIntersectionObserver.observe(img);
        });
      }
    } else {
      noWebpObserver(lazyImg);
    }
  }, 200);
};

var init = function init() {
  setScrollbar();
  pageInit();
  if (browserDetect) console.log("static");
  setTimeout(function () {
    html.classList.remove('loading');
  }, 200);

  if (!contain(html, "touch")) {
    console.log("desktop");
  } else {
    console.log("touch");
    setUnits();
    window.addEventListener('resize', function () {
      return setUnits();
    });

    if (contain(html, "mobile")) {
      console.log("mobile");
    }
  }
};

document.addEventListener('pjax:send', function () {
  return document.querySelector("#content").classList.add("fadeout");
});
document.addEventListener('pjax:success', function () {
  return pageInit();
});
document.addEventListener('DOMContentLoaded', init, false);