"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * Lazy Session History API
 * ===
 * Access the associated data of a history entry even after user navigations.
 *
 * On page navigation events (like popstate), `window.history.state` has already been changed and
 * we can't update the previous state anymore. To leave a last mark on the leaving page, we have to
 * either keep updating the state continuously - which usually causes performance issues,
 * or make use of other API.
 *
 * Internally, this module uses **session storage** to store data, and uses browsers' original
 * history state as keys to identify session storage items.
 */

/**
 * A valid history state object.
 */
var LazyHistory =
/*#__PURE__*/
function () {
  /**
   * The index of current state.
   */

  /**
   * The key used in `window.history.state` and session storage.
   */

  /**
   * The current state.
   */
  function LazyHistory(key) {
    _classCallCheck(this, LazyHistory);

    this.key = key;
    this.pull();
  }
  /**
   * Keep up with current browser history entry.
   */


  _createClass(LazyHistory, [{
    key: "pull",
    value: function pull() {
      // Get new state index.
      var historyState = window.history.state;
      var pulledIndex = historyState == null ? void 0 : historyState[this.key]; // Return if up-to-date.

      if (pulledIndex !== undefined && this.index === pulledIndex) return; // Get stored states.

      var stateListStr = window.sessionStorage.getItem(this.key);
      var stateList = stateListStr ? JSON.parse(stateListStr) : []; // Store current state.

      stateList[this.index] = this.state;
      window.sessionStorage.setItem(this.key, JSON.stringify(stateList));

      if (pulledIndex === undefined) {
        this.index = stateList.length;
        this.state = null;
        window.history.replaceState(_objectSpread({}, historyState, _defineProperty({}, this.key, this.index)), document.title);
      } else {
        this.index = pulledIndex;
        this.state = stateListStr ? stateList[pulledIndex] : null;
      }
    }
  }]);

  return LazyHistory;
}();
/**
 * Replace HTML contents by using innerHTML.
 */


var innerHTML = function innerHTML(oldNode, newNode) {
  // eslint-disable-next-line no-param-reassign
  oldNode.innerHTML = newNode.innerHTML;
};
/**
 * Replace all text by using textContent.
 */


var textContent = function textContent(oldNode, newNode) {
  // eslint-disable-next-line no-param-reassign
  oldNode.textContent = newNode.textContent;
};
/**
 * Replace readable text by using innerText.
 */


var innerText = function innerText(oldEle, newEle) {
  // eslint-disable-next-line no-param-reassign
  oldEle.innerText = newEle.innerText;
};
/**
 * Rewrite all attributes.
 */


var attributes = function attributes(oldEle, newEle) {
  var existingNames = oldEle.getAttributeNames();
  var targetNames = newEle.getAttributeNames();
  targetNames.forEach(function (target) {
    oldEle.setAttribute(target, newEle.getAttribute(target) || '');
    existingNames = existingNames.filter(function (existing) {
      return existing !== target;
    });
  });
  existingNames.forEach(function (existing) {
    oldEle.removeAttribute(existing);
  });
};
/**
 * Replace the whole element by using replaceWith.
 */


var replaceWith = function replaceWith(oldNode, newNode) {
  oldNode.replaceWith(newNode);
};

var Switches = {
  "default": replaceWith,
  innerHTML: innerHTML,
  textContent: textContent,
  innerText: innerText,
  attributes: attributes,
  replaceWith: replaceWith
};

var capitalize = function capitalize(str) {
  return "".concat(str.charAt(0).toUpperCase()).concat(str.slice(1));
};

var Submission =
/*#__PURE__*/
function () {
  /**
   * Parse the basic facilities that will be frequently used in the submission.
   * @see [Form submission algorithm | HTML Standard]{@link https://html.spec.whatwg.org/multipage/form-control-infrastructure.html#form-submission-algorithm}
   */
  function Submission(form, submitter) {
    _classCallCheck(this, Submission);

    this.form = form;
    this.submitButton = submitter;
  }
  /**
   * Parse submission related content attributes.
   * @see [Form submission attributes | HTML Standard]{@link https://html.spec.whatwg.org/multipage/form-control-infrastructure.html#form-submission-attributes}
   */


  _createClass(Submission, [{
    key: "getAttribute",
    value: function getAttribute(name) {
      var submitButton = this.submitButton,
          form = this.form;
      /**
       * Some attributes from the submit button override the form's one.
       * Before reading the IDL value, do a hasAttribute check since the IDL may return
       * a value (usually the default) even when the related content attribute is not present.
       */

      if (submitButton && submitButton.hasAttribute("form".concat(name))) {
        var overrideValue = submitButton["form".concat(capitalize(name))];
        if (overrideValue) return overrideValue;
      }

      return form[name];
    }
    /**
     * Construct the entry list and return in FormData format.
     * Manually append submitter entry before we can directly specify the submitter button.
     * The manual way has the limitation that the submitter entry always comes last.
     * @see [Constructing the entry list | HTML Standard]{@link https://html.spec.whatwg.org/multipage/form-control-infrastructure.html#constructing-form-data-set}
     * @see [FormData: Add ability to specify submitter in addition to &lt;form&gt; · whatwg/xhr]{@link https://github.com/whatwg/xhr/issues/262}
     */

  }, {
    key: "getEntryList",
    value: function getEntryList() {
      var form = this.form,
          submitButton = this.submitButton;
      var formData = new FormData(form);

      if (submitButton && !submitButton.disabled && submitButton.name) {
        formData.append(submitButton.name, submitButton.value);
      }

      return formData;
    }
    /**
     * The application/x-www-form-urlencoded and text/plain encoding algorithms
     * take a list of name-value pairs, where the values must be strings,
     * rather than an entry list where the value can be a File.
     * @see [Converting an entry list to a list of name-value pairs | HTML Standard]{@link https://html.spec.whatwg.org/multipage/form-control-infrastructure.html#converting-an-entry-list-to-a-list-of-name-value-pairs}
     */

  }, {
    key: "getNameValuePairs",
    value: function getNameValuePairs() {
      return Array.from(this.getEntryList(), function (_ref) {
        var _ref2 = _slicedToArray(_ref, 2),
            key = _ref2[0],
            value = _ref2[1];

        return [key, value instanceof File ? value.name : value];
      });
    }
    /**
     * URLSearchParams is a native API that
     * uses the application/x-www-form-urlencoded format and encoding algorithm.
     * @see [URLSearchParams class | URL Standard]{@link https://url.spec.whatwg.org/#interface-urlsearchparams}
     */

  }, {
    key: "getURLSearchParams",
    value: function getURLSearchParams() {
      return new URLSearchParams(this.getNameValuePairs());
    }
    /**
     * text/plain encoding algorithm for plain text form data.
     * @see [text/plain encoding algorithm | HTML Standard]{@link https://html.spec.whatwg.org/multipage/form-control-infrastructure.html#text/plain-encoding-algorithm}
     */

  }, {
    key: "getTextPlain",
    value: function getTextPlain() {
      return this.getNameValuePairs().reduce(function (str, _ref3) {
        var _ref4 = _slicedToArray(_ref3, 2),
            key = _ref4[0],
            value = _ref4[1];

        return "".concat(str).concat(key, "=").concat(value, "\r\n");
      }, '');
    }
    /**
     * Get the request to be sent by this submission.
     * @see [Form submission algorithm | HTML Standard]{@link https://html.spec.whatwg.org/multipage/form-control-infrastructure.html#form-submission-algorithm}
     */

  }, {
    key: "getRequestInfo",
    value: function getRequestInfo() {
      var action = this.getAttribute('action');
      var actionURL = new URL(action, document.baseURI); // Only 'http' and 'https' schemes are supported.

      if (!/^https?:$/.test(actionURL.protocol)) return null;

      switch (this.getAttribute('method')) {
        /**
         * Mutate action URL.
         * @see [Mutate action URL | HTML Standard]{@link https://html.spec.whatwg.org/multipage/form-control-infrastructure.html#submit-mutate-action}
         */
        case 'get':
          {
            actionURL.search = this.getURLSearchParams().toString();
            return actionURL.href;
          }

        /**
         * Submit as entity body.
         * @see [Submit as entity body | HTML Standard]{@link https://html.spec.whatwg.org/multipage/form-control-infrastructure.html#submit-body}
         */

        case 'post':
          {
            var body;

            switch (this.getAttribute('enctype')) {
              case 'application/x-www-form-urlencoded':
                body = this.getURLSearchParams();
                break;

              case 'multipart/form-data':
                body = this.getEntryList();
                break;

              case 'text/plain':
                body = this.getTextPlain();
                break;

              default:
                return null;
            }

            return new Request(action, {
              method: 'POST',
              body: body
            });
          }

        /**
         * Method with no request to send ('dialog' method) or unsupported.
         */

        default:
          return null;
      }
    }
  }]);

  return Submission;
}();
/**
 * Get the target browsing context chosen by anchors or forms
 * @see [The rules for choosing a browsing context | HTML Standard]{@link https://html.spec.whatwg.org/multipage/browsers.html#the-rules-for-choosing-a-browsing-context-given-a-browsing-context-name}
 */


var getBrowsingContext = function getBrowsingContext(target) {
  if (target === window.name) return window;

  switch (target.toLowerCase()) {
    case '':
    case '_self':
      return window;

    case '_parent':
      return window.parent;

    case '_top':
      return window.top;

    default:
      return undefined;
  }
};

var DefaultTrigger =
/*#__PURE__*/
function () {
  function DefaultTrigger(pjax) {
    _classCallCheck(this, DefaultTrigger);

    this.pjax = pjax;
  }
  /**
   * Check if the current trigger options apply to the element.
   */


  _createClass(DefaultTrigger, [{
    key: "test",
    value: function test(element) {
      var defaultTrigger = this.pjax.options.defaultTrigger;
      if (typeof defaultTrigger === 'boolean') return defaultTrigger;
      var enable = defaultTrigger.enable,
          exclude = defaultTrigger.exclude;
      return enable !== false && (!exclude || !element.matches(exclude));
    }
    /**
     * Load a resource with element attribute support.
     * @see [Follow the hyperlink | HTML Standard]{@link https://html.spec.whatwg.org/multipage/links.html#following-hyperlinks-2}
     * @see [Plan to navigate | HTML Standard]{@link https://html.spec.whatwg.org/multipage/form-control-infrastructure.html#plan-to-navigate}
     */

  }, {
    key: "load",
    value: function load(resource, subject) {
      var _subject$getAttribute, _subject$getAttribute2;
      /**
       * The RequestInit to align the request to send by the element.
       */


      var requestInit = {};
      /**
       * Referrer policy that specified on the element.
       * Will cause a TypeError in the later Request constructing step if the attribute is invalid.
       * Not bypassing forms here as it is supposed to be supported in the future.
       * @see [Add referrerpolicy to &lt;form&gt; · whatwg/html]{@link https://github.com/whatwg/html/issues/4320}
       */

      var referrerPolicy = (_subject$getAttribute = subject.getAttribute('referrerpolicy')) == null ? void 0 : _subject$getAttribute.toLowerCase();
      if (referrerPolicy !== undefined) requestInit.referrerPolicy = referrerPolicy;
      /**
       * Use no referrer if specified in the link types.
       * Not reading from `.relList` here as browsers haven't shipped it for forms yet.
       * @see [Add &lt;form rel&gt; initial compat data · mdn/browser-compat-data]{@link https://github.com/mdn/browser-compat-data/pull/9130}
       */

      if ((_subject$getAttribute2 = subject.getAttribute('rel')) != null && _subject$getAttribute2.split(/\s+/).some(function (type) {
        return type.toLowerCase() === 'noreferrer';
      })) {
        requestInit.referrer = '';
      }

      this.pjax.load(new Request(resource, requestInit))["catch"](function () {});
    }
  }, {
    key: "onLinkOpen",
    value: function onLinkOpen(event) {
      if (event.defaultPrevented) return;
      var target = event.target;
      if (!(target instanceof Element)) return;
      var link = target.closest('a[href], area[href]');
      if (!link) return;
      if (!this.test(link)) return;

      if (event instanceof MouseEvent || event instanceof KeyboardEvent) {
        if (event.metaKey || event.ctrlKey || event.shiftKey || event.altKey) return;
      }

      if (getBrowsingContext(link.target) !== window) return; // External.

      if (link.origin !== window.location.origin) return;
      event.preventDefault();
      this.load(link.href, link);
    }
  }, {
    key: "onFormSubmit",
    value: function onFormSubmit(event) {
      if (event.defaultPrevented) return;
      var form = event.target,
          submitter = event.submitter;
      if (!(form instanceof HTMLFormElement)) return;
      if (!this.test(form)) return;
      var submission = new Submission(form, submitter);
      if (getBrowsingContext(submission.getAttribute('target')) !== window) return;
      var requestInfo = submission.getRequestInfo();
      if (!requestInfo) return;
      var url = new URL(typeof requestInfo === 'string' ? requestInfo : requestInfo.url); // External.

      if (url.origin !== window.location.origin) return;
      event.preventDefault();
      this.load(requestInfo, form);
    }
  }, {
    key: "register",
    value: function register() {
      var _this = this;

      document.addEventListener('click', function (event) {
        _this.onLinkOpen(event);
      });
      document.addEventListener('keyup', function (event) {
        if (event.key !== 'Enter') return;

        _this.onLinkOpen(event);
      }); // Lacking browser compatibility and small polyfill. - August 2, 2021

      if ('SubmitEvent' in window) {
        document.addEventListener('submit', function (event) {
          _this.onFormSubmit(event);
        });
      }
    }
  }]);

  return DefaultTrigger;
}();

function switchNodes(sourceDocument, _ref5) {
  var selectors, switches, _ref5$signal, signal, focusCleared, switchPromises;

  return regeneratorRuntime.async(function switchNodes$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          selectors = _ref5.selectors, switches = _ref5.switches, _ref5$signal = _ref5.signal, signal = _ref5$signal === void 0 ? null : _ref5$signal;

          if (!(signal != null && signal.aborted)) {
            _context.next = 3;
            break;
          }

          throw new DOMException('Aborted switches', 'AbortError');

        case 3:
          focusCleared = false;
          switchPromises = [];
          selectors.forEach(function (selector) {
            var sourceNodeList = sourceDocument.querySelectorAll(selector);
            var targetNodeList = document.querySelectorAll(selector); // Throw when the structure is not match.

            if (sourceNodeList.length !== targetNodeList.length) {
              throw new DOMException("Selector '".concat(selector, "' does not select the same amount of nodes"), 'IndexSizeError');
            }

            var _document = document,
                activeElement = _document.activeElement; // Start switching for each match.

            targetNodeList.forEach(function (targetNode, index) {
              // Clear out focused controls before switching.
              if (!focusCleared && activeElement && targetNode.contains(activeElement)) {
                if (activeElement instanceof HTMLElement || activeElement instanceof SVGElement) {
                  activeElement.blur();
                }

                focusCleared = true;
              } // Argument defined switch is prior to default switch.


              var targetSwitch = (switches == null ? void 0 : switches[selector]) || Switches["default"]; // Start switching. Package to promise. Ignore switch errors.

              var switchPromise = Promise.resolve().then(function () {
                return targetSwitch(targetNode, sourceNodeList[index]);
              })["catch"](function () {});
              switchPromises.push(switchPromise);
            });
          }); // Reject as soon as possible on abort.

          _context.next = 8;
          return regeneratorRuntime.awrap(Promise.race([Promise.all(switchPromises), new Promise(function (resolve, reject) {
            signal == null ? void 0 : signal.addEventListener('abort', function () {
              reject(new DOMException('Aborted switches', 'AbortError'));
            });
          })]));

        case 8:
          return _context.abrupt("return", {
            focusCleared: focusCleared
          });

        case 9:
        case "end":
          return _context.stop();
      }
    }
  });
}

function switchDOM(requestInfo) {
  var _this2 = this;

  var overrideOptions,
      _this$abortController,
      _hooks$request,
      _this$options$overrid,
      selectors,
      switches,
      cache,
      timeout,
      hooks,
      eventDetail,
      signal,
      requestInit,
      rawRequest,
      request,
      timeoutID,
      _hooks$response,
      _hooks$document,
      _hooks$switchesResult,
      rawResponse,
      response,
      newLocation,
      rawDocument,
      _document2,
      rawSwitchesResult,
      switchesResult,
      _args2 = arguments;

  return regeneratorRuntime.async(function switchDOM$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          overrideOptions = _args2.length > 1 && _args2[1] !== undefined ? _args2[1] : {};
          _this$options$overrid = _objectSpread({}, this.options, {}, overrideOptions), selectors = _this$options$overrid.selectors, switches = _this$options$overrid.switches, cache = _this$options$overrid.cache, timeout = _this$options$overrid.timeout, hooks = _this$options$overrid.hooks;
          eventDetail = {};
          signal = ((_this$abortController = this.abortController) == null ? void 0 : _this$abortController.signal) || null;
          eventDetail.signal = signal;
          /**
           * Specify request cache mode and abort signal.
           */

          requestInit = {
            cache: cache,
            signal: signal
          };
          /**
           * Specify original referrer and referrerPolicy
           * since the later Request constructor steps discard the original ones.
           * @see [Request constructor steps | Fetch Standard]{@link https://fetch.spec.whatwg.org/#dom-request}
           */

          if (requestInfo instanceof Request) {
            requestInit.referrer = requestInfo.referrer;
            requestInit.referrerPolicy = requestInfo.referrerPolicy;
          }

          rawRequest = new Request(requestInfo, requestInit);
          rawRequest.headers.set('X-Requested-With', 'Fetch');
          rawRequest.headers.set('X-Pjax', 'true');
          rawRequest.headers.set('X-Pjax-Selectors', JSON.stringify(selectors));
          _context2.next = 13;
          return regeneratorRuntime.awrap((_hooks$request = hooks.request) == null ? void 0 : _hooks$request.call(hooks, rawRequest));

        case 13:
          _context2.t0 = _context2.sent;

          if (_context2.t0) {
            _context2.next = 16;
            break;
          }

          _context2.t0 = rawRequest;

        case 16:
          request = _context2.t0;
          eventDetail.request = request; // Set timeout.

          eventDetail.timeout = timeout;

          if (timeout > 0) {
            timeoutID = window.setTimeout(function () {
              var _this$abortController2;

              (_this$abortController2 = _this2.abortController) == null ? void 0 : _this$abortController2.abort();
            }, timeout);
            eventDetail.timeoutID = timeoutID;
          }

          this.fire('send', eventDetail);
          _context2.prev = 21;
          _context2.next = 24;
          return regeneratorRuntime.awrap(fetch(request)["finally"](function () {
            window.clearTimeout(timeoutID);
          }));

        case 24:
          rawResponse = _context2.sent;
          _context2.next = 27;
          return regeneratorRuntime.awrap((_hooks$response = hooks.response) == null ? void 0 : _hooks$response.call(hooks, rawResponse));

        case 27:
          _context2.t1 = _context2.sent;

          if (_context2.t1) {
            _context2.next = 30;
            break;
          }

          _context2.t1 = rawResponse;

        case 30:
          response = _context2.t1;
          eventDetail.response = response;
          this.fire('receive', eventDetail); // Push history state. Preserve hash as the fetch discards it.

          newLocation = new URL(response.url);
          newLocation.hash = new URL(request.url).hash;

          if (window.location.href !== newLocation.href) {
            window.history.pushState(null, '', newLocation.href);
          } // Switch elements.


          _context2.t2 = new DOMParser();
          _context2.next = 39;
          return regeneratorRuntime.awrap(response.text());

        case 39:
          _context2.t3 = _context2.sent;
          rawDocument = _context2.t2.parseFromString.call(_context2.t2, _context2.t3, 'text/html');
          _context2.next = 43;
          return regeneratorRuntime.awrap((_hooks$document = hooks.document) == null ? void 0 : _hooks$document.call(hooks, rawDocument));

        case 43:
          _context2.t4 = _context2.sent;

          if (_context2.t4) {
            _context2.next = 46;
            break;
          }

          _context2.t4 = rawDocument;

        case 46:
          _document2 = _context2.t4;
          eventDetail.switches = switches;
          _context2.next = 50;
          return regeneratorRuntime.awrap(switchNodes(_document2, {
            selectors: selectors,
            switches: switches,
            signal: signal
          }));

        case 50:
          rawSwitchesResult = _context2.sent;
          _context2.next = 53;
          return regeneratorRuntime.awrap((_hooks$switchesResult = hooks.switchesResult) == null ? void 0 : _hooks$switchesResult.call(hooks, rawSwitchesResult));

        case 53:
          _context2.t5 = _context2.sent;

          if (_context2.t5) {
            _context2.next = 56;
            break;
          }

          _context2.t5 = rawSwitchesResult;

        case 56:
          switchesResult = _context2.t5;
          eventDetail.switchesResult = switchesResult; // Simulate initial page load.

          _context2.next = 60;
          return regeneratorRuntime.awrap(this.preparePage(switchesResult, overrideOptions));

        case 60:
          _context2.next = 67;
          break;

        case 62:
          _context2.prev = 62;
          _context2.t6 = _context2["catch"](21);
          eventDetail.error = _context2.t6;
          this.fire('error', eventDetail);
          throw _context2.t6;

        case 67:
          _context2.prev = 67;
          this.fire('complete', eventDetail);
          return _context2.finish(67);

        case 70:
          this.fire('success', eventDetail);

        case 71:
        case "end":
          return _context2.stop();
      }
    }
  }, null, this, [[21, 62, 67, 70]]);
}
/**
 * Follow
 * https://html.spec.whatwg.org/multipage/scripting.html#prepare-a-script
 * excluding steps concerning obsoleted attributes.
 */

/**
 * Regex for JavaScript MIME type strings.
 * @see [JavaScript MIME type | MIME Sniffing Standard]{@link https://mimesniff.spec.whatwg.org/#javascript-mime-type}
 */


var MIMETypeRegex = /^((application|text)\/(x-)?(ecma|java)script|text\/(javascript1\.[0-5]|(j|live)script))$/;

var Script =
/*#__PURE__*/
function () {
  function Script(scriptEle) {
    _classCallCheck(this, Script);

    this.external = false;
    this.blocking = false;
    this.evaluable = false;
    this.target = scriptEle; // Process empty.

    if (!scriptEle.hasAttribute('src') && !scriptEle.text) return; // Process type.

    var typeString = scriptEle.type ? scriptEle.type.trim().toLowerCase() : 'text/javascript';

    if (MIMETypeRegex.test(typeString)) {
      this.type = 'classic';
    } else if (typeString === 'module') {
      this.type = 'module';
    } else {
      return;
    } // Process no module.


    if (scriptEle.noModule && this.type === 'classic') {
      return;
    } // Process external.


    if (scriptEle.src) {
      this.external = true;
    } // Process blocking.
    // It's minifier plugins' job to merge conditions. We split them out for readability.


    this.blocking = true;

    if (this.type !== 'classic') {
      this.blocking = false;
    } else if (this.external) {
      /**
       * The async IDL attribute may not reflect the async content attribute.
       * @see [The async IDL attribute | HTML Standard]{@link https://html.spec.whatwg.org/multipage/scripting.html#dom-script-async}
       */
      if (scriptEle.hasAttribute('async')) {
        this.blocking = false;
      } else if (scriptEle.defer) {
        this.blocking = false;
      }
    }

    this.evaluable = true;
  }

  _createClass(Script, [{
    key: "eval",
    value: function _eval() {
      var _this3 = this;

      return new Promise(function (resolve, reject) {
        var oldEle = _this3.target;
        var newEle = document.createElement('script');
        newEle.addEventListener('error', reject);
        /**
         * Clone attributes and inner text.
         * Reset async since it defaults to true on dynamically created scripts.
         */

        newEle.async = false;
        oldEle.getAttributeNames().forEach(function (name) {
          newEle.setAttribute(name, oldEle.getAttribute(name) || '');
        });
        newEle.text = oldEle.text;
        /**
         * Execute.
         * Not using `.isConnected` here as it is also `true`
         * for scripts connected in other documents.
         */

        if (document.contains(oldEle)) {
          oldEle.replaceWith(newEle);
        } else {
          // Execute in <head> if it's not in current document.
          document.head.append(newEle);

          if (_this3.external) {
            newEle.addEventListener('load', function () {
              return newEle.remove();
            });
          } else {
            newEle.remove();
          }
        }

        if (_this3.external) {
          newEle.addEventListener('load', function () {
            return resolve();
          });
        } else {
          resolve();
        }
      });
    }
  }]);

  return Script;
}();

var Executor =
/*#__PURE__*/
function () {
  function Executor(signal) {
    _classCallCheck(this, Executor);

    this.signal = signal;
  }
  /**
   * Execute script.
   * Throw only when aborted.
   * Wait only for blocking script.
   */


  _createClass(Executor, [{
    key: "exec",
    value: function exec(script) {
      var _this$signal, evalPromise;

      return regeneratorRuntime.async(function exec$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              if (!((_this$signal = this.signal) != null && _this$signal.aborted)) {
                _context3.next = 2;
                break;
              }

              throw new DOMException('Execution aborted', 'AbortError');

            case 2:
              evalPromise = script.eval()["catch"](function () {});

              if (!script.blocking) {
                _context3.next = 6;
                break;
              }

              _context3.next = 6;
              return regeneratorRuntime.awrap(evalPromise);

            case 6:
            case "end":
              return _context3.stop();
          }
        }
      }, null, this);
    }
  }]);

  return Executor;
}();
/**
 * Find and execute scripts in order.
 * Needed since innerHTML does not run scripts.
 */


function executeScripts(scriptEleList) {
  var _ref6,
      _ref6$signal,
      signal,
      validScripts,
      executor,
      execution,
      _args4 = arguments;

  return regeneratorRuntime.async(function executeScripts$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _ref6 = _args4.length > 1 && _args4[1] !== undefined ? _args4[1] : {}, _ref6$signal = _ref6.signal, signal = _ref6$signal === void 0 ? null : _ref6$signal;

          if (!(signal != null && signal.aborted)) {
            _context4.next = 3;
            break;
          }

          throw new DOMException('Aborted execution', 'AbortError');

        case 3:
          validScripts = Array.from(scriptEleList, function (scriptEle) {
            return new Script(scriptEle);
          }).filter(function (script) {
            return script.evaluable;
          });
          executor = new Executor(signal); // Evaluate external scripts first
          // to help browsers fetch them in parallel.
          // Each inline blocking script will be evaluated as soon as
          // all its previous blocking scripts are executed.

          execution = validScripts.reduce(function (promise, script) {
            if (script.external) {
              return Promise.all([promise, executor.exec(script)]);
            }

            return promise.then(function () {
              return executor.exec(script);
            });
          }, Promise.resolve()); // Reject as soon as possible on abort.

          _context4.next = 8;
          return regeneratorRuntime.awrap(Promise.race([execution, new Promise(function (resolve, reject) {
            signal == null ? void 0 : signal.addEventListener('abort', function () {
              reject(new DOMException('Aborted execution', 'AbortError'));
            });
          })]));

        case 8:
        case "end":
          return _context4.stop();
      }
    }
  });
}
/**
 * Get the indicated part of the document.
 * Not using :target pseudo class here as it may not be updated by pushState.
 * @see [The indicated part of the document | HTML Standard]{@link https://html.spec.whatwg.org/multipage/browsing-the-web.html#the-indicated-part-of-the-document}
 */


var getIndicatedPart = function getIndicatedPart() {
  var target = null;
  var hashId = decodeURIComponent(window.location.hash.slice(1));
  if (hashId) target = document.getElementById(hashId) || document.getElementsByName(hashId)[0];
  if (!target && (!hashId || hashId.toLowerCase() === 'top')) target = document.scrollingElement;
  return target;
};
/**
 * After page elements are updated.
 */


function preparePage(switchesResult) {
  var overrideOptions,
      options,
      _this$abortController,
      autofocus,
      scripts,
      scrollTo,
      parsedScrollTo,
      target,
      _args5 = arguments;

  return regeneratorRuntime.async(function preparePage$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          overrideOptions = _args5.length > 1 && _args5[1] !== undefined ? _args5[1] : {};
          options = _objectSpread({}, this.options, {}, overrideOptions); // If page elements are switched.

          if (!switchesResult) {
            _context5.next = 10;
            break;
          }

          // Focus the FIRST autofocus if the previous focus is cleared.
          // https://html.spec.whatwg.org/multipage/interaction.html#the-autofocus-attribute
          if (switchesResult.focusCleared) {
            autofocus = document.querySelectorAll('[autofocus]')[0];

            if (autofocus instanceof HTMLElement || autofocus instanceof SVGElement) {
              autofocus.focus();
            }
          } // List newly added and labeled scripts.


          scripts = [];

          if (options.scripts) {
            document.querySelectorAll(options.scripts).forEach(function (element) {
              if (element instanceof HTMLScriptElement) scripts.push(element);
            });
          }

          options.selectors.forEach(function (selector) {
            document.querySelectorAll(selector).forEach(function (element) {
              if (element instanceof HTMLScriptElement) {
                scripts.push(element);
              } else {
                element.querySelectorAll('script').forEach(function (script) {
                  if (scripts.includes(script)) return;
                  scripts.push(script);
                });
              }
            });
          }); // Sort in document order.
          // https://stackoverflow.com/a/22613028

          scripts.sort(function (a, b) {
            return (// Bitwise AND operator is required here.
              // eslint-disable-next-line no-bitwise
              a.compareDocumentPosition(b) & Node.DOCUMENT_POSITION_PRECEDING || -1
            );
          }); // Execute.

          _context5.next = 10;
          return regeneratorRuntime.awrap(executeScripts(scripts, {
            signal: ((_this$abortController = this.abortController) == null ? void 0 : _this$abortController.signal) || null
          }));

        case 10:
          // Parse required scroll position.
          scrollTo = options.scrollTo; // When scroll is allowed.

          if (scrollTo !== false) {
            // If switched, default to left top. Otherwise, default to no scroll.
            parsedScrollTo = switchesResult ? [0, 0] : false;

            if (Array.isArray(scrollTo)) {
              parsedScrollTo = scrollTo;
            } else if (typeof scrollTo === 'number') {
              parsedScrollTo = [window.scrollX, scrollTo];
            } else {
              target = getIndicatedPart();

              if (target) {
                target.scrollIntoView();
                parsedScrollTo = false;
              }
            } // Scroll.


            if (parsedScrollTo) window.scrollTo(parsedScrollTo[0], parsedScrollTo[1]);
          }

        case 12:
        case "end":
          return _context5.stop();
      }
    }
  }, null, this);
}
/**
 * Load a URL in Pjax way. Throw all errors.
 */


function weakLoad(requestInfo) {
  var overrideOptions,
      _this$abortController,
      abortController,
      url,
      path,
      currentPath,
      _args6 = arguments;

  return regeneratorRuntime.async(function weakLoad$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          overrideOptions = _args6.length > 1 && _args6[1] !== undefined ? _args6[1] : {};
          // Store scroll position.
          this.storeHistory(); // Setup abort controller.

          abortController = new AbortController();
          (_this$abortController = this.abortController) == null ? void 0 : _this$abortController.abort();
          this.abortController = abortController;
          /**
           * The URL object of the target resource.
           * Used to identify fragment navigations.
           */

          url = new URL(typeof requestInfo === 'string' ? requestInfo : requestInfo.url, document.baseURI);
          path = url.pathname + url.search;
          currentPath = this.location.pathname + this.location.search;
          /**
           * Identify fragment navigations.
           * Not using `.hash` here as it becomes the empty string for both empty and null fragment.
           * @see [Navigate fragment step | HTML Standard]{@link https://html.spec.whatwg.org/multipage/browsing-the-web.html#navigate-fragid-step}
           * @see [URL hash getter | URL Standard]{@link https://url.spec.whatwg.org/#dom-url-hash}
           */

          if (!(path === currentPath && url.href.includes('#'))) {
            _context6.next = 14;
            break;
          }

          // pushState on different hash.
          if (window.location.hash !== url.hash) {
            window.history.pushState(null, '', url.href);
          } // Directly prepare for fragment navigation.


          _context6.next = 12;
          return regeneratorRuntime.awrap(this.preparePage(null, overrideOptions));

        case 12:
          _context6.next = 16;
          break;

        case 14:
          _context6.next = 16;
          return regeneratorRuntime.awrap(this.switchDOM(requestInfo, overrideOptions));

        case 16:
          // Update Pjax location and prepare the page.
          this.history.pull();
          this.location.href = window.location.href; // Finish, remove abort controller.

          this.abortController = null;

        case 19:
        case "end":
          return _context6.stop();
      }
    }
  }, null, this);
}

var Pjax =
/*#__PURE__*/
function () {
  _createClass(Pjax, null, [{
    key: "reload",
    value: function reload() {
      window.location.reload();
    }
    /**
     * Options default values.
     */

  }]);

  function Pjax() {
    var _this4 = this;

    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    _classCallCheck(this, Pjax);

    this.options = {
      defaultTrigger: true,
      selectors: ['title', '.pjax'],
      switches: {},
      scripts: 'script[data-pjax]',
      scrollTo: true,
      scrollRestoration: true,
      cache: 'default',
      timeout: 0,
      hooks: {}
    };
    this.history = new LazyHistory('pjax');
    this.location = new URL(window.location.href);
    this.abortController = null;
    this.switchDOM = switchDOM;
    this.preparePage = preparePage;
    this.weakLoad = weakLoad;
    Object.assign(this.options, options);

    if (this.options.scrollRestoration) {
      window.history.scrollRestoration = 'manual'; // Browsers' own restoration is faster and more stable on reload.

      window.addEventListener('beforeunload', function () {
        window.history.scrollRestoration = 'auto';
      });
    }

    var defaultTrigger = this.options.defaultTrigger;

    if (defaultTrigger === true || defaultTrigger !== false && defaultTrigger.enable !== false) {
      new DefaultTrigger(this).register();
    }

    window.addEventListener('popstate', function (event) {
      /**
       * The main reason why we write the LazyHistory library is right here:
       * `window.history.state` is ALREADY changed on popstate events and
       * we can't update the previous state anymore. (For scroll position, etc.)
       * As continuously updating `window.history.state` causes performance issues,
       * using a custom library seems to be the only choice.
       */
      // Store scroll position and then update the lazy state.
      _this4.storeHistory();

      _this4.history.pull(); // hashchange events trigger popstate with a null `event.state`.


      if (event.state === null) return;
      var overrideOptions = {};

      if (_this4.options.scrollRestoration && _this4.history.state) {
        overrideOptions.scrollTo = _this4.history.state.scrollPos;
      }

      _this4.load(window.location.href, overrideOptions)["catch"](function () {});
    });
  }

  _createClass(Pjax, [{
    key: "storeHistory",
    value: function storeHistory() {
      this.history.state = {
        scrollPos: [window.scrollX, window.scrollY]
      };
    }
    /**
     * Fire Pjax related events.
     */

  }, {
    key: "fire",
    value: function fire(type, detail) {
      var event = new CustomEvent("pjax:".concat(type), {
        bubbles: true,
        cancelable: false,
        detail: _objectSpread({
          abortController: this.abortController
        }, detail)
      });
      document.dispatchEvent(event);
    }
    /**
     * Load a URL in Pjax way. Navigate normally on errors except AbortError.
     */

  }, {
    key: "load",
    value: function load(requestInfo) {
      var overrideOptions,
          _args7 = arguments;
      return regeneratorRuntime.async(function load$(_context7) {
        while (1) {
          switch (_context7.prev = _context7.next) {
            case 0:
              overrideOptions = _args7.length > 1 && _args7[1] !== undefined ? _args7[1] : {};
              _context7.prev = 1;
              _context7.next = 4;
              return regeneratorRuntime.awrap(this.weakLoad(requestInfo, overrideOptions));

            case 4:
              _context7.next = 11;
              break;

            case 6:
              _context7.prev = 6;
              _context7.t0 = _context7["catch"](1);

              if (!(_context7.t0 instanceof DOMException && _context7.t0.name === 'AbortError')) {
                _context7.next = 10;
                break;
              }

              throw _context7.t0;

            case 10:
              window.location.assign(typeof requestInfo === 'string' ? requestInfo : requestInfo.url);

            case 11:
            case "end":
              return _context7.stop();
          }
        }
      }, null, this, [[1, 6]]);
    }
  }]);

  return Pjax;
}();

exports["default"] = Pjax;
Pjax.switches = Switches;