import Pjax from './pjax.esm.js';

// GLOBAL FUNCTIONS

const html = document.documentElement;
const body = document.body;

const contain = ( e, slug ) => e.classList.contains( slug );

function modifyState( url ) {
    let stateObj = { id: "100" };
    window.history.replaceState( stateObj, "", url );
}

const figureIntersectionObserver = new IntersectionObserver(

    allObserved => {

        allObserved.forEach( observed => {

            if ( !observed.isIntersecting ) return;

            const img = observed.target;

            img.src = img.dataset.src;
            delete img.dataset.src;
            delete img.dataset.oc;
            img.classList.remove( "lazy" );

            figureIntersectionObserver.unobserve( img );

        } );

    }

);

const browserDetect = () => {

    var userAgent = navigator.userAgent;

    return userAgent.match(/safari/i) && !userAgent.match(/chrome|chromium|crios/i)

}

const setScrollbar = () => {

    // Create the measurement node
    var scrollDiv = document.createElement("div");
        scrollDiv.className = "scrollbar-measure";

    body.appendChild( scrollDiv );

    var sb = scrollDiv.offsetWidth - scrollDiv.clientWidth;

    html.style.setProperty( '--sb', `${ sb }px` );

    body.removeChild( scrollDiv );

}

// POLYFILL FUNCTIONS

const pureAJAX = async ( e ) => {

    var target = e.target;
    var href = target.href;

    let request = new XMLHttpRequest();
        request.open( "GET", `${ href }`, true );
        request.onload = () => {

            const targetHtml = new DOMParser().parseFromString( request.responseText, 'text/html' );

            html.querySelector( `#content` ).innerHTML = targetHtml.querySelector( `#content` ).innerHTML;

            modifyState( href );

            const mainTitle = html.querySelector( `h1` );
            const nextTitle = targetHtml.querySelector( `#content h2` );

            if ( nextTitle ) document.title = `${ mainTitle.innerText } — ${ nextTitle.innerText }`;

        }
        request.send();

    setTimeout( () => pageInit, 1000 );

}

const noWebpObserver = e => {

    e.forEach( img => {

        img.src = img.dataset.oc;
        delete img.dataset.src;
        delete img.dataset.oc;
        img.classList.remove( "lazy" );

    });

};

const setUnits = () => {

    let vh = window.innerHeight * 0.01;
    html.style.setProperty( '--vh', `${ vh }px` );

};

const orthoTypo = str => {

	if ( typeof str === "object" ) str = str.innerText;
  
    const ent = '&#8239;';

    const stArr = [
        [ ' ;', `${ ent };` ],
        [ ' :', `${ ent }:` ],
        [ ' !', `${ ent }!` ],
        [ ' ?', `${ ent }?` ],
        [ '« ', `«${ ent }` ],
        [ ' »', `${ ent }»` ],
        [ ' & ', ` &amp;&nbsp;` ],
    ];

    for ( let i = 0; i < stArr.length; i++ ) {
        const e = stArr[ i ];

        if ( str.includes( e[ 0 ] ) ) str = str.replaceAll( e[ 0 ], e[ 1 ] );
    }

    return str;
  
}

// GLOBAL INIT

const pageInit = () => {
    
    if ( !contain( html, "touch" ) ) {

        console.log( "desktop" );

        const pjax = new Pjax( {

            elements: "a",
            selectors: [ "#content" ],

        } );

        console.log( pjax );

    } else {
        
        if ( contain( html, "mobile" ) ) {

            console.log( "mobile" );

            var links = document.querySelectorAll( "a" );

            for (let i = 0; i < links.length; i++) {

                const link = links[i];

                link.addEventListener( "click", e => pureAJAX( e ) )
                
            }

        } else {

            const pjax = new Pjax( {

                elements: "a",
                selectors: [ "#content" ],

            } );

        }

    }

    setTimeout( () => {

        const lazyImg = document.querySelectorAll( ".lazy" );
    
        if ( contain( html, "webp" ) ) {

            if ( lazyImg.length !== 0 && window.IntersectionObserver ) {

                lazyImg.forEach( img => figureIntersectionObserver.observe( img ) );
                
            }

        } else {

            noWebpObserver( lazyImg );

        }
        
    }, 200);

}

const init = () => {

    setScrollbar();

    pageInit();

    if ( browserDetect ) console.log( "static" );

    setTimeout(() => {
        
        html.classList.remove( 'loading' );

    }, 200);
    
    if ( !contain( html, "touch" ) ) {

        console.log("desktop");

    } else {

        console.log("touch");

        setUnits();

        window.addEventListener( 'resize', () => setUnits() );
        
        if ( contain( html, "mobile" ) ) {

            console.log("mobile");

        }

    }

}

document.addEventListener( 'pjax:send', () => document.querySelector( "#content" ).classList.add( "fadeout" ) );

document.addEventListener( 'pjax:success', () => pageInit() );

document.addEventListener( 'DOMContentLoaded', init, false );