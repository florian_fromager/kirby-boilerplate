<nav>
    <a href="<?= $site->url() ?>"><h2><?= $site->title() ?></h2></a>
    <?php foreach( $site->children()->listed() as $page ): ?>
        <a href="<?= $page->url() ?>"><h2><?= $page->title() ?></h2></a> 
    <?php endforeach ?>
</nav>