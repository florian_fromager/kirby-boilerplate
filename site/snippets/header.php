<?php

    $base_url = $site->urlForLanguage( "en" );
    $assets = "$base_url/assets";

    snippet( 'functions' );

    $detect = $page->detect();

?><!DOCTYPE html>
<html lang="<?= currentLang( $kirby ) ?>" dir="ltr" class="loading <?= $page->isMobile() ? ( $page->isTablet() ? "touch tablet" : "touch mobile" ) : null ?>">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="robots" content="index">

        <title><?= $site->title() ?></title>
        
        <?= "<link rel='canonical' href='".$site->url()."' />" ?>
    
        <?= $site->desc()->isNotEmpty() ? "<meta name='description' content='".str_replace( "'", "&#39;", $site->desc()->html() )."' />" : null ?>
        <?= $site->tags()->isNotEmpty() ? "<meta name='keywords' content='".$site->tags()."' />" : null ?>

        <meta property="og:type" content="website" />
        <?= $site->title()->isNotEmpty() ? "<meta property='og:title' content='".str_replace( "'", "&#39;", $site->title()->html() )."' />" : null ?>
        <?= $site->desc()->isNotEmpty() ? "<meta property='og:description' content='".str_replace( "'", "&#39;", $site->desc()->html() )."' />" : null ?>
        <?= $site->cover()->isNotEmpty() ? "<meta property='og:image' content='".$site->cover()->toFile()->url()."' />" : null ?>
        <?= "<meta property='og:url' content='".$site->url()."' />" ?>
        <?= $site->title()->isNotEmpty() ? "<meta property='og:site_name' content='".$site->title()->html()."' />" : null ?>

        <link rel="icon" type="image/svg+xml" href="<?= $assets ?>/favicon/favicon.svg">
        <link rel="alternate icon" href="<?= $assets ?>/favicon/favicon.ico">
        <link rel="mask-icon" href="<?= $assets ?>/favicon/safari-pinned-tab.svg" color="#fff">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= $assets ?>/favicon/favicon.svg">
        <link rel="icon" type="image/png" sizes="32x32" href="<?= $assets ?>/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?= $assets ?>/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?= $assets ?>/favicon/site.webmanifest">
        <link rel="mask-icon" href="<?= $assets ?>/favicon/favicon.svg" color="#fff">
        <meta name="msapplication-TileColor" content="#fff">
        <meta name="theme-color" content="#fff">

        <link rel="preload" href="<?= $assets ?>/fonts/capriola-regular-webfont.woff2" as="font" type="font/woff2" crossorigin>

        <link rel="preload" href="<?= $assets ?>/css/index.min.css" as="style">
        <link rel="stylesheet" href="<?= $assets ?>/css/index.min.css">
        <?php
            if( $page->isMobile() ) {
                echo "<link rel='preload' href='$base_url/assets/css/mobile.min.css' as='style'>";
                echo "<link rel='stylesheet' href='$base_url/assets/css/mobile.min.css'>";
            }
        ?>

        <link rel="preload" href="<?= $assets ?>/dist/modernizr-custom.js" as="script">
        <link rel="preload" href="<?= $assets ?>/dist/bundle.js" as="script">
    </head>
    <body>

        <div class="container">