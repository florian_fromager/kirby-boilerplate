<?php

/** @var \Kirby\Cms\Block $block */

$alt = $block->alt();
$caption = $block->caption();
$crop = $block->crop()->isTrue();
$link = $block->link();
$alt = $block->alt();

$ratio = $block->ratio();

$src = null;

if ( $block->location() == 'web' ) {

    $src = $block->src()->esc();
    $img_ratio = null;

} elseif ( $img = $block->image()->toFile() ) {

    $alt = $alt ?? $img->alt();
    $src = $img->url();
    $img_ratio = $img->ratio();

}

?>

<?php if ( $src ): ?>
    <?php if ( $ratio->isNotEmpty() ): ?>
    <figure <?= $ratio != 'auto' && calc_ratio( $ratio ) < 1 ? 'class="small"' : null ?>>
        <?= myToWebp( $site, $img, $alt->esc(), $ratio ) ?>
        <?php if ( $caption->isNotEmpty() ): ?>
            <figcaption><?= $caption ?></figcaption>
        <?php endif; ?>
    </figure>
    <?php else: ?>
    <figure <?= $img_ratio != null && $img_ratio < 1 ? 'class="small"' : null ?>>
        <?= myToWebp( $site, $img, $alt->esc(), null ) ?>
        <?php if ( $caption->isNotEmpty() ): ?>
            <figcaption><?= $caption ?></figcaption>
        <?php endif; ?>
    </figure>
    <?php endif; ?>
<?php endif; ?>