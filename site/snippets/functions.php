<?php

    function currentLang( $e ) {

        return $e->languageCode();

    }

    function myToWebp( $site, $e, $alt, $aspect_ratio ) {

        $oc = "data-oc='".$e->url()."'";

        if ( $e->extension() != 'webp' && $e->extension() != 'gif' ) {

            $name = $e->name();
            $file = $e->parent()->files()->find( "$name.webp" );

            if ( $file != null ) $e = $file;

        }

        $lazy = $e->url();

        if( $e->extension() != 'gif' ) {

            if ( $site->isTablet() ) {
    
                $e = $e->orientation() == "landscape" ? $e->resize( 1280 ) : $e->resize( 840 );
    
            } elseif ( $site->isMobile() ) {
    
                $e = $e->orientation() == "landscape" ? $e->resize( 840 ) : $e->resize( 480 );
    
            }

        }

        $lazy = $lazy->url();
        $url = $e->url();
        $width = $e->width();
        $height = $e->height();

        return "<img src='$lazy' data-src='$url' $oc height='$height' width='$width' style='aspect-ratio:$width/$height;' alt='$alt' class='lazy' >";

    }