    </div><?php 
    
        $base_url = $site->urlForLanguage( "en" );
        $assets = "$base_url/assets";
    
    ?>
    <script src="<?= $assets ?>/dist/modernizr-custom.js"></script>
    <script src="<?= $assets ?>/dist/bundle.js"></script>
</body>
</html>