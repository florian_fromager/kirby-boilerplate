<?php

return [

    'debug' => true,

    'panel' => [

        'slug' => 'dashboard',

    ],

    'languages' => true,
    'languages.detect' => true,

    'medienbaecker.autoresize.maxWidth' => 2048,
    'medienbaecker.autoresize.maxHeight' => 2048,
    'medienbaecker.autoresize.quality' => 90,

    'kirby3-webp' => true,

    'sylvainjule.matomo.url'        => 'http://your-matomo.url',
    'sylvainjule.matomo.id'         => 'mywebsite',
    'sylvainjule.matomo.token'      => 'token_auth',

];