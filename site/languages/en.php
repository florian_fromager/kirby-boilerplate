<?php

    return [
        'code' => 'en',
        'default' => true,
        'direction' => 'ltr',
        'locale' => 'en_US.utf8',
        'name' => 'English',
        'url' => '/'
    ];