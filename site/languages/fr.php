<?php

    return [
        'code' => 'fr',
        'direction' => 'ltr',
        'locale' => 'fr_FR.utf8',
        'name' => 'Français',
        'url' => '/fr'
    ];