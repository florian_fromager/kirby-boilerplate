# Kirby boilerplate

<img src="http://getkirby.com/assets/images/github/plainkit.jpg" width="300">

**Kirby: the CMS that adapts to any project, loved by developers and editors alike.** 


## Description
Kirby's CMS base for work

## Versions
- Kirby: 3.7.0.1
- autoresize: 2.1.2
- WebP: 0.0.6
- Mobile Detect: 1.3.4
- colors: 1.5.2
- matomo: 1.0.7
- modernizr: 3.6.0

## Installation
- You only have to clone the directory on your computer and use it.
- CSS in assets is compiled from SASS.
- viewport unit polyfill for mobile. (https://css-tricks.com/the-trick-to-viewport-units-on-mobile/)
- All JS in the assets is bundled with webpack.
- Page transition using [PaperStrike"s Pjax 2.4.0](https://github.com/PaperStrike/pjax)

---

© 2009-2020 Bastian Allgeier (Bastian Allgeier GmbH)  
[getkirby.com](https://getkirby.com) · [License agreement](https://getkirby.com/license)